#include <stdint.h>
#include "main.h"
#include "gpio_config.h"

void set_logic (uint8_t logic);

void write_code (uint8_t id_data, uint8_t cmd_data);

void write_data (uint8_t id_data, uint8_t add_data, uint16_t data_data);

void ht1632_init (void);
