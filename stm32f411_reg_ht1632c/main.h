#define set_pin_A4 GPIOA->BSRR |= (1<<4) 
#define reset_pin_A4 GPIOA->BSRR |= ((1<<4) <<16)
#define set_pin_A5 GPIOA->BSRR |= (1<<5) 
#define reset_pin_A5 GPIOA->BSRR |= ((1<<5) <<16)
#define set_pin_A6 GPIOA->BSRR |= (1<<6) 
#define reset_pin_A6 GPIOA->BSRR |= ((1<<6) <<16)
#define set_pin_A7 GPIOA->BSRR |= (1<<7) 
#define reset_pin_A7 GPIOA->BSRR |= ((1<<7) <<16)
#define set_pin_C13 GPIOC->BSRR |= (1<<13) 
#define reset_pin_C13 GPIOC->BSRR |= ((1<<13) <<16)

#define set_pin_CS	reset_pin_A4
#define reset_pin_CS	set_pin_A4
#define set_pin_CLK	set_pin_A5
#define reset_pin_CLK	reset_pin_A5
#define set_pin_DATA	set_pin_A7
#define reset_pin_DATA	reset_pin_A7

#define id_read 0x06	// read
#define id_write 0x05	// write
#define id_cmd 0x04		// command

#define code_sys_dis 0x00
#define code_sys_en 0x01
#define code_led_off 0x02
#define code_led_on 0x03
#define code_blynk_off 0x08
#define code_blynk_on 0x09
#define code_slave_mode 0x10
#define code_master_mode 0x18
#define code_ext_clk_mode 0x1C
#define code_com_nmos_8 0x20
#define code_com_nmos_16 0x24
#define code_com_pmos_8 0x28
#define code_com_pmos_16 0x2C
#define code_pwm_1_16 0xA0
#define code_pwm_2_16 0xA1
#define code_pwm_3_16 0xA2
#define code_pwm_4_16 0xA3
#define code_pwm_5_16 0xA4
#define code_pwm_6_16 0xA5
#define code_pwm_7_16 0xA6
#define code_pwm_8_16 0xA7
#define code_pwm_9_16 0xA8
#define code_pwm_10_16 0xA9
#define code_pwm_11_16 0xAA
#define code_pwm_12_16 0xAB
#define code_pwm_13_16 0xAC
#define code_pwm_14_16 0xAD
#define code_pwm_15_16 0xAE
#define code_pwm_16_16 0xAF

#define n_0 0x003F	// 0b 0000 0000 0011 1111
#define n_1 0x0406	// 0b 0000 0100 0000 0110
#define n_2 0x00DB	// 0b 0000 0000 1101 1011
#define n_3 0x008F	// 0b 0000 0000 1000 1111
#define n_4 0x00E6	// 0b 0000 0000 1110 0110
#define n_5 0x00ED	// 0b 0000 0000 1110 1101
#define n_6 0x00FD	// 0b 0000 0000 1111 1101
#define n_7 0x1401	// 0b 0001 0100 0000 0001
#define n_8 0x00FF	// 0b 0000 0000 1111 1111
#define n_9 0x00E7	// 0b 0000 0000 1110 0111
#define n_ 0x00C0		// 0b 0000 0000 1100 0000
#define n_d 0x0482	// 0b 0000 0100 1000 0010
#define n_A 0x00F7	// 0b 0000 0000 1111 0111
#define n_B 0x128F	// 0b 0001 0010 1000 1111
#define n_C 0x0039	// 0b 0000 0000 0011 1001
#define n_D 0x120F	// 0b 0001 0010 0000 1111
#define n_E 0x00F9	// 0b 0000 0000 1111 1001
#define n_F 0x00F1	// 0b 0000 0000 1111 0001
#define n_G 0x00BD	// 0b 0000 0000 1011 1101
#define n_H 0x00F6	// 0b 0000 0000 1111 0110
#define n_I 0x1209	// 0b 0001 0010 0000 1001
#define n_J 0x001E	// 0b 0000 0000 0001 1110
#define n_K 0x0C70	// 0b 0000 1100 0111 0000
#define n_L 0x0038	// 0b 0000 0000 0011 1000
#define n_M 0x0536	// 0b 0000 0101 0011 0110
#define n_N 0x0936	// 0b 0000 1001 0011 0110
#define n_O 0x003F	// 0b 0000 0000 0011 1111
#define n_P 0x00F3	// 0b 0000 0000 1111 0011
#define n_Q 0x083F	// 0b 0000 1000 0011 1111
#define n_R 0x08F3	// 0b 0000 1000 1111 0011
#define n_S 0x018D	// 0b 0000 0001 1000 1101
#define n_T 0x1201	// 0b 0001 0010 0000 0001
#define n_U 0x003E	// 0b 0001 0010 0011 1110
#define n_V 0x2430	// 0b 0010 0100 0011 0000
#define n_W 0x2836	// 0b 0010 1000 0011 0110
#define n_X 0x2D00	// 0b 0010 1101 0000 0000
#define n_Y 0x1500	// 0b 0001 0101 0000 0000
#define n_Z 0x2409	// 0b 0010 0100 0000 1001

#define sym_bt 0x0080	// row 0
#define sym_R 0x0040	// row 0
#define sym_fm 0x0080	// row 1
#define sym_bazz 0x0040	// row 1
#define sym_card 0x0080	// row 2
#define sym_mhz 0x0040	// row 2
#define sym_usb 0x0080	// row 3
#define sym_dotf 0x0040	// row 3
#define sym_wifi 0x0080	// row 4
#define sym_fotb 0x0040	// row 4
#define sym_music 0x0080	// row 5
#define sym_dot 0x0040 	// row 5
#define sym_video 0x0080	// row 6
#define sym_pic 0x0080	// row 7
#define sym_play 0x0080	// row 8
#define sym_pause 0x0080 // row 9
#define sym_repeat 0x0080	// row 10
#define sym_rand 0x0080	// row 11
#define sym_mic 0x0080	// row 12
#define sym_L 0x0080	// row 13

#define set_play segment[8] |= sym_play
#define set_repeat segment[10] |= sym_repeat
#define set_mic segment[12] |= sym_mic
#define set_bt segment[0] |= sym_bt
#define set_fm segment[1] |= sym_fm
#define set_card segment[2] |= sym_card
#define set_usb segment[3] |= sym_usb
#define set_wifi segment[4] |= sym_wifi
#define set_music segment[5] |= sym_music
#define set_video segment[6] |= sym_video
#define set_picture segment[7] |= sym_pic
#define set_pause segment[9] |= sym_pause
#define set_rand segment[11] |= sym_rand
#define set_L segment[13] |= sym_L
#define set_R segment[0] |= sym_R
#define set_bazz segment[1] |= sym_bazz
#define set_mhz segment[2] |= sym_mhz